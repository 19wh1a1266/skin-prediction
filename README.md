# INTRODUCTION

Skin diseases are the common health problems in the worldwide.

They are mostly caused by fungal infection, bacteria, allergy, or viruses, etc.

There are 3000 and more unknown skin diseases.

The medical equipments for such diagnosis is limited and most expensive.

# problem statement

Deep learning techniques helps in detection of skin disease at an initial stage.

The feature extraction and data reconstruction plays a key role in classification of skin diseases.

A Dataset of 938 images has been taken for the Classification of Skin diseases.

By using CNN algorithms, 70 percentage of accuracy is achieved in classification of skin diseases


# Methodology
The first model involves collection of dataset, the images are collected from ISIC dataset (International Skin Imaging Collaboration) 

It also  involves  the pre-processing of the images where hair removal, glare removal and shading removal are done.

Removal  of  these parameters  helps us  to identify  the texture,  color,  size  and  shape  like  parameters  in  an efficient way.  


# CNN algorithm
A Convolutional Neural Network (ConvNet/CNN) is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other. 

The pre-processing required in a ConvNet is much lower as compared to other classification algorithms. 

While in primitive methods filters are hand-engineered, with enough training, ConvNets have the ability to learn these filters/characteristics.

model=Sequential([
    Conv2D(32,(3,3),input_shape=(img_height,img_width,3),activation='relu',padding='same'),
    Conv2D(32,(3,3),activation='relu'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2,2)),
    Dropout(0.25),
    
    Conv2D(64,(3,3),activation='relu',padding='same'),
    Conv2D(64,(3,3),activation='relu'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2,2)),
    Dropout(0.25),
    
    Flatten(),
    Dense(32, activation='relu'),
    Dense(9, activation='softmax')
])


#Data Preprocessing
-Working with preprocessing layers:
-Keras preprocessing layers
   -Core preprocessing layers
      -Dense layer
      -Normalization layer
-Image preprocessing layers
   -Rescaling layer
-Pooling layers

